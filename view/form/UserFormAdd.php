<div id="content">
    <form method="post" action="">
        <fieldset>
            <legend>Add User</legend>
            <label>Username *:</label>
            <input type="text" placeholder="Username" name="username" value="<?php if (isset($content)) { echo $content->getUsername(); } ?>" />
            <label>Password *:</label>
            <input type="password" placeholder="Password" name="password" value="<?php if (isset($content)) { echo $content->getPassword(); } ?>" />
            <label>Age :</label>
            <input type="number" placeholder="Age" name="age" value="<?php if (isset($content)) { echo $content->getAge(); } ?>" />
            <label>Role *:</label>
            <select name="role">
                <option value="">---------</option>
                <option value="<?php if (isset($content)) { $selected = ""; echo $content->getRole(); } ?>">Basic</option>
                <option value="<?php if (isset($content)) { $selected = ""; echo $content->getRole(); } ?>">Advanced</option>             
            </select>
            <input type="hidden" value="1" name="active">
            
            <label>* Required fields</label>
            <input type="submit" name="action" value="add" />
        </fieldset>
    </form>
</div>