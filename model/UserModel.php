<?php

require_once "model/persist/UserFileDAO.php";
require_once "model/UserModel.php";


class UserModel {

    private $dataUser;

    public function __construct() {
        // File
        $this->dataUser = UserFileDao::getInstance();

        // Database
        //$this->dataCategory=CategoryDbDAO::getInstance();
    }

    /**
     * insert a category
     * @param $category Category object to insert
     * @return TRUE or FALSE
     */
    public function add($user): bool {
        $result = $this->dataUser->add($user);

        if ($result == FALSE) {
            $_SESSION['error'] = UserMessage::ERR_DAO['insert'];
        }

        return $result;
    }

    /**
     * update a category
     * @param $category Category object to update
     * @return TRUE or FALSE
     */
    public function modify($category): bool {
        $result = $this->dataCategory->modify($category);

        if (!$result) {
            $_SESSION['error'] = CategoryMessage::ERR_DAO['update'];
        }
        return $result;
    }

    /**
     * delete a category
     * @param $id string Category Id to delete
     * @return TRUE or FALSE
     */
    public function delete($id): bool {

        $result = false;
        $productModel = new ProductModel();
        $categoryUsed = $productModel->categoryInProduct($id);

        if (!$categoryUsed) {
            $result = $this->dataCategory->delete($id);

            if (!$result) {
                $_SESSION['error'] = CategoryMessage::ERR_DAO['delete'];
            }
            
        } else {
            $_SESSION['error'] = CategoryMessage::ERR_DAO['used'];
        }

        return $result;
    }

    /**
     * select all categories
     * @param void
     * @return array of Category objects or array void
     */
    public function listAll(): array {
        $users = $this->dataUser->listAll();
        
        return $users;
    }

    /**
     * select a category by Id
     * @param $id string Category Id
     * @return Category object or NULL
     */
    public function searchById($username) {
        $result = $this->dataUser->searchById($username);


        return $result;
    }

}


