<?php


class User {
    private $username;
    private $password;
    private $age;
    private $role;
    private $active;
    
    
    function __construct($username = NULL, $password= NULL, $age= NULL, $role= NULL, $active= NULL) {
        $this->username = $username;
        $this->password = $password;
        $this->age = $age;
        $this->role = $role;
        $this->active = $active;
    }
    
    
    function getUsername() {
        return $this->username;
    }

    function getPassword() {
        return $this->password;
    }

    function getAge() {
        return $this->age;
    }

    function getRole() {
        return $this->role;
    }

    function getActive() {
        return $this->active;
    }

    function setUsername($username): void {
        $this->username = $username;
    }

    function setPassword($password): void {
        $this->password = $password;
    }

    function setAge($age): void {
        $this->age = $age;
    }

    function setRole($role): void {
        $this->role = $role;
    }

    function setActive($active): void {
        $this->active = $active;
    }


    public function __toString() {
        return sprintf("%s;%s;%s;%s;%s\n", $this->username, $this->password, $this->age, $this->role, $this->active);

    }



}
