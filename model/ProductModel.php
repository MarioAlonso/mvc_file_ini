<?php

require_once 'model/ModelInterface.class.php';
require_once 'model/persist/ProductFileDAO.php';

class ProductModel implements ModelInterface {

    //put your code here

    private $dataProduct;

    public function __construct() {
        $this->dataProduct = ProductFileDAO::getInstance();
    }

    public function add($object): bool {
        $result = $this->dataProduct->add($object);
        if ($result == FALSE) {
            $_SESSION['error'] = CategoryMessage::ERR_DAO['insert'];
        }

        return $result;
    }

    public function delete($id): bool {

        $result = $this->dataProduct->delete($id);


        if (!$result) {
            $_SESSION['error'] = ProductMessage::ERR_DAO['delete'];
        } 

        return $result;
    }

    public function listAll($categories): array {
        return $this->dataProduct->listAll($categories);
    }

    public function modify($object): bool {
        $result = $this->dataProduct->modify($object);

        if (!$result) {
            $_SESSION['error'] = ProductMessage::ERR_DAO['update'];
        }
        return $result;
    }

    public function searchById($id) {
        $result = $this->dataProduct->searchById($id);


        return $result;
    }

    public function listCategories() {
        $categoryModel = new CategoryModel;
        $categories = $categoryModel->listAll();
        return $categories;
    }

    public function categoryInProduct($idCategory): bool {
        $result = $this->dataProduct->categoryInProduct($idCategory);

        return $result;
    }

    public function listProduct() {
        
    }

}
