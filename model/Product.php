<?php

class Product {

    private $id;
    private $name;
    private $price;
    private $description;
    private $category;

    function __construct($id = NULL, $name = NULL, $price = NULL, $description = NULL, $category = NULL) {
        $this->id = $id;
        $this->name = $name;
        $this->price = $price;
        $this->description = $description;
        $this->category = $category;

    }

    function getId() {
        return $this->id;
    }

    function getName() {
        return $this->name;
    }

    function setId($id): void {
        $this->id = $id;
    }

    function setName($name): void {
        $this->name = $name;
    }
    
    function getPrice() {
        return $this->price;
    }

    function getDescription() {
        return $this->description;
    }

    function getCategory() {
        return $this->category;
    }

    function setPrice($price): void {
        $this->price = $price;
    }

    function setDescription($description): void {
        $this->description = $description;
    }

    function setCategory($category): void {
        $this->category = $category;
    }


    public function __toString() {
        return sprintf("%s;%s;%s;%s;%s\n", $this->id, $this->name, $this->price, $this->description, $this->category);
    }

}
