<?php

require_once "model/ModelInterface.class.php";
require_once "model/persist/ConnectFile.class.php";

class ProductFileDAO implements ModelInterface {

    //put your code here
    private static $instance = NULL; // instancia de la clase
    private $connect; // conexión actual

    const FILE = "model/resource/products.txt";

    public function __construct() {
        $this->connect = new ConnectFile(self::FILE);
    }

    // singleton: patrón de diseño que crea una instancia única
    // para proporcionar un punto global de acceso y controlar
    // el acceso único a los recursos físicos
    public static function getInstance(): ProductFileDAO {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function add($product): bool {
        $result = FALSE;
        // abre el fichero en modo append
        if ($this->connect->openFile("a+")) {
            fputs($this->connect->getHandle(), $product->__toString());
            $this->connect->closeFile();
            $result = TRUE;
        }

        return $result;
    }

    public function delete($id): bool {
        $result = False;
        $fileData = array();

        //abre el fichero en modo read
        if ($this->connect->openFile("r")) {
            while (!feof($this->connect->getHandle())) {
                $line = trim(fgets($this->connect->getHandle()));
                if ($line != "") {
                    $fields = explode(";", $line);
                    if ($id != $fields[0]) {
                        array_push($fileData, $line . "\n");
                    }
                }
            }
            $this->connect->closeFile();
        }

        if ($this->connect->writeFile($fileData)) {
            $result = TRUE;
        }
        return $result;
    }

    public function listAll($categories): array {
        $result = array();
        $arr_index_category = ProductFileDAO::getIndexCategory($categories);
        //abrir el fichero en modo lectura
        if ($this->connect->openFile("r")) {
            while (!feof($this->connect->getHandle())) {
                $line = trim(fgets($this->connect->getHandle()));
                if ($line != "") {
                    $fields = explode(";", $line);
                    $product = new Product($fields[0], $fields[1], $fields[2], $fields[3], $arr_index_category[$fields[4]]);
                    array_push($result, $product);
                }
            }
            $this->connect->closeFile();
        }
        return $result;
    }

    public function modify($object): bool {
        $result = False;
        $fileData = array();

        //abre el fichero en modo read
        if ($this->connect->openFile("r")) {
            while (!feof($this->connect->getHandle())) {
                $line = trim(fgets($this->connect->getHandle()));
                if ($line != "") {
                    $fields = explode(";", $line);
                    if ($object->getId() == $fields[0]) {
                        //Cambiar y poner la nueva categoria
                        array_push($fileData, $object->__toString());
                        break;
                    } else {
                        array_push($fileData, $line . "\n");
                    }
                }
            }
            $this->connect->closeFile();
        }

        if ($this->connect->writeFile($fileData)) {
            $result = TRUE;
        }
        return $result;
    }

    public function searchById($id) {
        $product = NULL;

        //abre el fichero en modo read
        if ($this->connect->openFile("r")) {
            while (!feof($this->connect->getHandle())) {
                $line = trim(fgets($this->connect->getHandle()));
                if ($line != "") {
                    $fields = explode(";", $line);

                    if ($id == $fields[0]) {
                        $product = new Product($fields[0], $fields[1], $fields[2], $fields[3], $fields[4]);
                        break;
                    }
                }
            }
            $this->connect->closeFile();
        }
        return $product;
    }

    public function categoryInProduct($idCategory): bool {
        $result = False;

        if ($this->connect->openFile("r")) {
            while (!feof($this->connect->getHandle())) {
                $line = trim(fgets($this->connect->getHandle()));
                if ($line != "") {
                    $fields = explode(";", $line);
                    if ($idCategory == $fields[4]) {
                        $result = true;
                        break;
                    }
                }
            }
            $this->connect->closeFile();
        }
        return $result;
    }
    
    public function getIndexCategory($categories) {
        $arr_index_category = array();
        foreach ($categories as $category) {
            $arr_index_category[$category->getId()] = $category->getName();
        }
        return $arr_index_category;
    }

}
