<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserMessage
 *
 * @author marioalonso
 */
class UserMessage {

    const INF_FORM = array(
        'insert' => 'Data inserted successfully',
        'update' => 'Data updated successfully',
        'delete' => 'Data deleted successfully',
        'found' => 'Data found'
    );
    const ERR_FORM = array(
        'empty_username' => 'Username must be filled',
        'empty_password' => 'Password must be filled',
        'empty_name' => 'Name must be filled',
        'invalid_username' => 'Username must be valid values',
        'invalid_password' => 'Password must be valid values',
        'exists_username' => 'Username already exists',
        'not_exists_id' => 'Id not exists',
        'not_found' => 'No data found',
        'invalid_price' => 'Price no valid',
        'invalid_age' => 'Age no valid',
        'invalid_category' => 'Category no valid'
    );
    const ERR_DAO = array(
        'insert' => 'Error inserting data',
        'update' => 'Error updating data',
        'delete' => 'Error deleting data',
        'used' => 'No data deleted, Category in use',
        '' => ''
    );

}
