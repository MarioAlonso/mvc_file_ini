<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserFormValidation
 *
 * @author marioalonso
 */
class UserFormValidation {
    const ADD_FIELDS = array('username','password','age','role');
    const MODIFY_FIELDS = array('username','password');
    const DELETE_FIELDS = array('username');
    const SEARCH_FIELDS = array('username');
    
    const NUMERIC = "/[^0-9]/";
    const ALPHABETIC = "/[^a-z A-Z]/";
    
    public static function checkData($fields) {
        $username=NULL;
        $password=NULL;
        $age=NULL;
        $role=NULL;

        
        foreach ($fields as $field) {
            switch ($field) {
                case 'username':                    
                    $username=trim(filter_input(INPUT_POST, 'username'));
                    $userValid=!preg_match(self::ALPHABETIC, $username);
                    if (empty($username)) {
                        array_push($_SESSION['error'], UserMessage::ERR_FORM['empty_username']);
                    }
                    else if ($userValid == FALSE) {
                        array_push($_SESSION['error'], UserMessage::ERR_FORM['invalid_username']);
                    }                    
                    
                    break;
                case 'password':
                    $password=trim(filter_input(INPUT_POST, 'password'));
                    $passValid=filter_var($password, FILTER_SANITIZE_STRING);
                    if (empty($password)) {
                        array_push($_SESSION['error'], UserMessage::ERR_FORM['empty_password']);
                    }
                    else if ($passValid == FALSE) {
                        array_push($_SESSION['error'], UserMessage::ERR_FORM['invalid_password']);
                    }
                    break;
                case 'age':
                    // filter_var retorna los datos filtrados o FALSE si el filtro falla
                    $age=trim(filter_input(INPUT_POST, 'age'));
                    $ageValid=!preg_match(self::NUMERIC, $age);
                    if ($ageValid == FALSE || $age < 18) {
                        array_push($_SESSION['error'], UserMessage::ERR_FORM['invalid_age']);
                    }
                    break;
                case 'xx':
                    // filter_var retorna los datos filtrados o FALSE si el filtro falla
                    $id=trim(filter_input(INPUT_POST, 'id'));
                    $idValid=filter_var($id, FILTER_SANITIZE_NUMBER_INT);
                    if ($idValid == FALSE) {
                        array_push($_SESSION['error'], CategoryMessage::ERR_FORM['invalid_id']);
                    }
                    break;
                case 'xx':
                    $name=trim(filter_input(INPUT_POST, 'name'));
                    $nameValid=filter_var($name, FILTER_SANITIZE_STRING);
                    if ($nameValid == FALSE) {
                        array_push($_SESSION['error'], CategoryMessage::ERR_FORM['invalid_name']);
                    }
                    break;
            }
        }
        
        
        $user=new User($username, $password,$age,$role);


        
        return $user;
    }}
