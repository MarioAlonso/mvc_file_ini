<?php

require_once "controller/ControllerInterface.php";
require_once "view/ProductView.php";
require_once "model/ProductModel.php";
require_once "model/Product.php";
require_once "util/ProductMessage.php";
require_once "util/ProductFormValidation.php";

class ProductController implements ControllerInterface {

    //put your code here
    private $view;
    private $model;

    function __construct() {
        $this->view = new ProductView();
        $this->model = new ProductModel();
    }

    public function processRequest() {

        $request = NULL;
        $_SESSION['info'] = array();
        $_SESSION['error'] = array();

        $request = filter_has_var(INPUT_POST, 'action') ? filter_input(INPUT_POST, 'action') : NULL;

        if (!$request) {
            $request = filter_has_var(INPUT_GET, 'option') ? filter_input(INPUT_GET, 'option') : NULL;
        }

        switch ($request) {
            case "form_add":
                $this->formAdd();
                break;
            case "list_all":
                $this->listAll();
                break;
            case "add":
                $this->add();
                break;
            case "form_modify":
                $this->form_modify();
                break;
            case "modify":
                $this->modify();
                break;
            case "search":
                $this->searchById();
                break;
            case "delete":
                $this->delete();
                break;
            default:
                $this->view->display();
        }
    }

    public function formAdd() {
        $categories = $this->model->listCategories();

        $this->view->display("view/form/ProductFormAdd.php", null, $categories);
    }

    public function add() {
        $productValid = ProductFormValidation::checkData(ProductFormValidation::ADD_FIELDS);
        if (empty($_SESSION['error'])) {
            $product = $this->model->searchById($productValid->getId());
            if (is_null($product)) {
                $result = $this->model->add($productValid);
                if ($result == TRUE) {
                    $_SESSION['info'] = ProductMessage::INF_FORM['insert'];
                    $productValid = NULL;
                }
            } else {
                $_SESSION['error'] = ProductMessage::ERR_FORM['exists_id'];
            }
        }

        $categories = $this->model->listCategories();
        $this->view->display("view/form/ProductFormAdd.php", $productValid, $categories);
    }

    public function delete() {
        $productValid = ProductFormValidation::checkData(ProductFormValidation::DELETE_FIELDS);

        if (empty($_SESSION['error'])) {
            $product = $this->model->searchById($productValid->getId());

            if (!is_null($product)) {
                $result = $this->model->delete($productValid->getId());

                if ($result == TRUE) {
                    $_SESSION['info'] = ProductMessage::INF_FORM['delete'];
                    $productValid = NULL;
                }
            } else {
                $_SESSION['error'] = ProductMessage::ERR_FORM['not_exists_id'];
            }
        }

        $this->view->display("view/form/ProductFormModify.php", $productValid);
    }

   public function listAll() {
        $categories = $this->model->listCategories();
        $products = $this->model->listAll($categories);
        //$products = array();

        if (!empty($products)) { // array void or array of Product objects?
            $_SESSION['info'] = ProductMessage::INF_FORM['found'];
        } else {
            $_SESSION['error'] = ProductMessage::ERR_FORM['not_found'];
        }
        
        $this->view->display("view/form/ProductList.php", $products);
    }

    public function form_modify() {
        $categories = $this->model->listCategories();
        $this->view->display("view/form/ProductFormModify.php", null, $categories);
    }

    public function modify() {
        $productValid = ProductFormValidation::checkData(ProductFormValidation::MODIFY_FIELDS);

        if (empty($_SESSION['error'])) {
            $product = $this->model->searchById($productValid->getId());

            if (!is_null($product)) {
                $result = $this->model->modify($productValid);

                if ($result == TRUE) {
                    $_SESSION['info'] = ProductMessage::INF_FORM['update'];
                }
            } else {
                $_SESSION['error'] = ProductMessage::ERR_FORM['not_exists_id'];
            }
        }

        $categories = $this->model->listCategories();
        $this->view->display("view/form/ProductFormModify.php", $productValid, $categories);
    }

    public function searchById() {
        $productValid = ProductFormValidation::checkData(ProductFormValidation::SEARCH_FIELDS);

        if (empty($_SESSION['error'])) {
            $product = $this->model->searchById($productValid->getId());

            if (!is_null($product)) { // is NULL or Category object?
                $_SESSION['info'] = ProductMessage::INF_FORM['found'];
                $productValid = $product;
            } else {
                $_SESSION['error'] = ProductMessage::ERR_FORM['not_found'];
            }
        }

        $categories = $this->model->listCategories();
        $this->view->display("view/form/ProductFormModify.php", $productValid, $categories);
    }

}
