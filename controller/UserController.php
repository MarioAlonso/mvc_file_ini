<?php

require_once "controller/ControllerInterface.php";
require_once "view/UserView.php";
require_once "model/UserModel.php";
require_once "model/User.php";
require_once "util/UserMessage.php";
require_once "util/UserFormValidation.php";

class UserController {

//put your code here
    private $view;
    private $model;

    function __construct() {
        $this->view = new UserView();
        $this->model = new UserModel();
    }

    public function processRequest() {

        $request = NULL;
        $_SESSION['info'] = array();
        $_SESSION['error'] = array();

        $request = filter_has_var(INPUT_POST, 'action') ? filter_input(INPUT_POST, 'action') : NULL;

        if (!$request) {
            $request = filter_has_var(INPUT_GET, 'option') ? filter_input(INPUT_GET, 'option') : NULL;
        }
        if (isset($_SESSION['username'])) {
            switch ($request) {
                case "form_add":
                    $this->formAdd();
                    break;
                case "list_all":
                    $this->listAll();
                    break;
                case "add":
                    $this->add();
                    break;
                case "form_search":
                    $this->formSearch();
                    break;
                case "search":
                    $this->searchById();
                    break;
                case "delete":
                    $this->delete();
                    break;
                default:
                    $this->view->display();
            }
            
        } else {
            switch ($request) {
                case "login":
                    $this->login();
                    break;
                default:
                    $this->view->display("view/form/LoginForm.php");
            }
        }
    }

    public function formAdd() {
        $this->view->display("view/form/UserFormAdd.php");
    }

    public function formSearch() {
        $this->view->display("view/form/UserFormSearch.php");
    }

    // ejecuta la acción de insertar categoría
    public function add() {
        $userValid = UserFormValidation::checkData(UserFormValidation::ADD_FIELDS);

        if (empty($_SESSION['error'])) {
            $user = $this->model->searchById($userValid->getUsername());

            if (is_null($user)) {
                $result = $this->model->add($userValid);

                if ($result == TRUE) {
                    $_SESSION['info'] = UserMessage::INF_FORM['insert'];
                    $userValid = NULL;
                }
            } else {
                $_SESSION['error'] = UserMessage::ERR_FORM['exists_username'];
            }
        }

        $this->view->display("view/form/UserFormAdd.php", $userValid);
    }

    // ejecuta la acción de mostrar todas las categorías
    public function listAll() {
        $users = $this->model->listAll();

        if (!empty($users)) { // array void or array of Category objects?
            $_SESSION['info'] = UserMessage::INF_FORM['found'];
        } else {
            $_SESSION['error'] = UserMessage::ERR_FORM['not_found'];
        }

        $this->view->display("view/form/UserList.php", $users);
    }

    // ejecuta la acción de buscar categoría por id de categoría
    public function searchById() {
        $userValid = UserFormValidation::checkData(UserFormValidation::SEARCH_FIELDS);
        $user = NULL;
        if (empty($_SESSION['error'])) {
            $user = $this->model->searchById($userValid->getUsername());

            if (!is_null($user)) { // is NULL or Category object?
                $_SESSION['info'] = UserMessage::INF_FORM['found'];
                $userValid = $user;
            } else {
                $_SESSION['error'] = UserMessage::ERR_FORM['not_found'];
            }
        }


        if (!is_null($user)) {
            $this->view->display("view/form/UserFormModifyDelete.php", $userValid);
        } else {
            $this->view->display("view/form/UserFormModify.php", $userValid);
        }
    }

    // carga el formulario de buscar productos por nombre de categoría
    public function formListProducts() {
        $this->view->display("view/form/CategoryFormSearchProduct.php");
    }

    public function delete() {
        $userValid = UserFormValidation::checkData(UserFormValidation::DELETE_FIELDS);

        if (empty($_SESSION['error'])) {
            $user = $this->model->searchById($userValid->getUsername());

            if (!is_null($user)) {
                $result = $this->model->delete($userValid->getId());

                if ($result == TRUE) {
                    $_SESSION['info'] = UserMessage::INF_FORM['delete'];
                    $userValid = NULL;
                }
            } else {
                $_SESSION['error'] = UserMessage::ERR_FORM['not_exists_id'];
            }
        }

        $this->view->display("view/form/UserFormModify.php", $userValid);
    }

    // ejecuta la acción de buscar productos por nombre de categoría
    public function listProducts() {
        $name = trim(filter_input(INPUT_POST, 'name'));

        $result = NULL;
        if (!empty($name)) { // Category Name is void?
            $result = $this->model->listProducts($name);

            if (!empty($result)) { // array void or array of Product objects?
                $_SESSION['info'] = "Data found";
            } else {
                $_SESSION['error'] = CategoryMessage::ERR_FORM['not_found'];
            }

            $this->view->display("view/form/CategoryListProduct.php", $result);
        } else {
            $_SESSION['error'] = CategoryMessage::ERR_FORM['invalid_name'];

            $this->view->display("view/form/CategoryFormSearchProduct.php", $result);
        }
    }

}
